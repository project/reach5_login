REACH5 LOGIN

INTRODUCTION
------------

This module create a new block embeding the files necessary for displaying the reachfive login module.


REQUIREMENTS
------------

This module need the firebase/JWT libraries:
https://github.com/firebase/php-jwt


CONFIGURATION
-------------

You can configure the module in: admin/config/services/reachfive

In the config page you can type all the reach5 access code, select some options for the login.

You can map the reach5 result with some user field.

The output of the module can overriden for more flexibility.

You can alter the reach5 tokenIdPayload to made it compatible with your user fields.
    hook_reach5_tokenID_payload_alter($tokenIdPayload){
        //here we check if the gender is part of the response and modify the value to be compatible with our field value
        if( $tokenIdPayload->gender ) $tokenIdPayload->gender = ($tokenIdPayload->gender == 'male') ? 'homme' : 'femme';
    }
More info about the reach5 response value:
  https://developer.reach5.co/api/identity-web-legacy/#id-token-payload